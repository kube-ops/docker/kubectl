ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

FROM busybox:1.32 AS builder

ARG VERSION

RUN set -eux \
  ; export TEST_VERSION=$(echo "$VERSION" | tr -d '[0-9]') \
  ; if [ -z "$TEST_VERSION" ]; then export VERSION="1.19.3"; fi \
  ; wget -O kubectl "https://storage.googleapis.com/kubernetes-release/release/v${VERSION}/bin/linux/amd64/kubectl" \
  ; chmod 0755 kubectl \
  ; mv -vf kubectl /bin/kubectl \
  ; kubectl version --client --short

FROM quay.io/kube-ops/app-base:latest

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

LABEL \
  org.label-schema.build-date="${BUILD_DATE}" \
  org.label-schema.name="kubectl" \
  org.label-schema.description="Kubernetes client" \
  org.label-schema.url="https://gitlab.com/kube-ops/docker/kubectl" \
  org.label-schema.vcs-ref="${VCS_REF}" \
  org.label-schema.version="${VERSION}" \
  org.label-schema.vcs-url="https://gitlab.com/kube-ops/docker/kubectl" \
  org.label-schema.vendor="Anton Kulikov" \
  org.label-schema.schema-version="1.0"

COPY --from=builder /bin/kubectl /app/bin/kubectl
ENTRYPOINT ["kubectl"]
CMD ["--help"]
